
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let sut: LoginComponent;

  beforeEach(() => {
    sut = new LoginComponent();
  });

  it('set show registration dialog as true', () => {
    const initialResult: boolean = sut.displayRegistrationDialog;

    sut.showRegistrationDialog();

    const afterResult: boolean = sut.displayRegistrationDialog;

    expect(initialResult).toEqual(false);
    expect(afterResult).toEqual(true);
  });
});
