import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../shared/login/login.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  displayRegistrationDialog = false;
  loginFormControl = new FormGroup({
    userName: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private readonly loginService: LoginService,
  ) { }

  ngOnInit(): void {
  }

  showRegistrationDialog(): void {
    this.displayRegistrationDialog = true;
  }

  attemptLogin(): void {
    if (this.loginFormControl.status === 'VALID') {
      this.loginService.authenticate(this.loginFormControl.controls.userName.value, this.loginFormControl.controls.password.value);
    }
  }
}
